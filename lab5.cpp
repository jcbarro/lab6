/*
Joseph Barron
CPSC 1021- 001
Feb 15, 2019
Lab 5
*/


#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"

using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs); //important for sorting

int myrandom (int i) { return std::rand()%i;} // used to make shuffle the cards in the random_suffle function



int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


// fix the values of the cards they are all printing out the value of 14


  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
int i = 0;
Card card[52]; 


while (i < 52) { //loop for making every one of the 52 cards

 if (i < 13) { //spades loop. first 13 cards
   for (int j = 2; j < 15; j++) {
    card[i].suit = SPADES;
    card[i].value = j; 
    i++;
   }
  }

 if (i > 12 && i < 26) { // hearts loop.
   for (int j = 2; j < 15; j++) {
    card[i].suit = HEARTS;
    card[i].value = j; 
    i++;
   }
  }

 if (i > 25 && i < 49) { // loop for the Diamonds.
   for (int j = 2; j < 15; j++) {
    card[i].suit = DIAMONDS;
    card[i].value = j;
    i++; 
   }
  }

 if (i > 48 && i < 52) { // last 13 cards which are Clubs
   for (int j = 2; j < 15; j++) {
    card[i].suit = CLUBS;
    card[i].value = j;
    i++; 
   }
  }

}



  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
random_shuffle(&card[0], &card[52],myrandom);




   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
Card arr[5] = {card[0], card[1], card[2], card[3], card[4]}; //put the first three cards of the shuffled deck into an array of 5 





    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
sort(&arr[0],&arr[5], suit_order); //sending to sort the hand of cards from least to greatest
 








    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
for (int j =0; j<5; j++) {
	cout << setw(10) << get_card_name(arr[j]) << " of " << get_suit_code(arr[j]) << endl; // loop to print out the card values, and call functions 
}
 //cout<< setw(10) << left << endl;


  return 0;
}






/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit < rhs.suit) {
    return true;
   }
 else if(lhs.suit == rhs.suit) {
    if (lhs.value < rhs.value) {
	 return true;
	}
}
    return false;
   
	}
string get_suit_code(Card& c) 
 {
  switch (c.suit) 
   {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
   }
 }

string get_card_name(Card& c) 
 {
  // IMPLEMENT

  if (c.value < 11) {
	return to_string(c.value); }
  else if (c.value == 11) { //setting the 11 value to Jack
	return "Jack"; } 
  else if (c.value == 12) { //setting the 12 value to Queen 
	return "Queen"; } 
  else if (c.value == 13) { //setting the 13 value to King
	return "King"; } 
  else if (c.value == 14) { //setting the 14 value to Ace
	return "Ace"; } 

  return " ";
 }
